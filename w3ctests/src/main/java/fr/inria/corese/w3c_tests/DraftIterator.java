package fr.inria.corese.w3c_tests;

import fr.inria.corese.core.load.Load;
import fr.inria.corese.core.load.LoadException;
import fr.inria.corese.core.query.QueryProcess;
import fr.inria.corese.kgram.core.Mapping;
import fr.inria.corese.kgram.core.Mappings;
import fr.inria.corese.core.Graph;
import fr.inria.corese.sparql.exceptions.EngineException;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Stream;

public class DraftIterator {
    public static String READ_TEST_WITHOUT_UPDATE = "prefix mf:  <http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#> "
            + "prefix qt:  <http://www.w3.org/2001/sw/DataAccess/tests/test-query#> "
            + "prefix sd:  <http://www.w3.org/ns/sparql-service-description#> "
            + "prefix ent: <http://www.w3.org/ns/entailment/> "
            + "prefix dawgt: <http://www.w3.org/2001/sw/DataAccess/tests/test-dawg#> "
            + "select  * where {"
            + "?x mf:action ?a "
            + "optional {?x mf:name ?nameTest} "
            + "optional {?x rdfs:comment ?commentTest} "
            + "minus {?x dawgt:approval dawgt:Withdrawn}"
            + "optional {?a qt:query ?q} "
            + "optional {?a qt:data ?d}"
            + "optional {?a qt:graphData ?g} "
            + "optional {?a sd:entailmentRegime ?ent}"
            + "optional {?x sd:entailmentRegime ?ent}"
            + "optional {?x mf:result ?r}"
            + "optional {?x rdf:type ?t}"
            + "optional {?x mf:feature ?fq}"
            + "optional { ?x mf:recognizedDatatypes/rdf:rest*/rdf:first ?rdt }"
            + "optional { ?x mf:unrecognizedDatatypes/rdf:rest*/rdf:first ?udt }"
            + "optional { ?a qt:serviceData [ qt:endpoint ?endpoint ; qt:data ?endpointdata ] }"
            + "{?man rdf:first ?x} "
            + "} "
            + "group by ?x order by ?q ";
    public static String READ_TEST_WITH_UPDATE = "prefix mf:  <http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#> "
            + "prefix ut:     <http://www.w3.org/2009/sparql/tests/test-update#> "
            + "prefix sd:  <http://www.w3.org/ns/sparql-service-description#> "
            + "prefix ent: <http://www.w3.org/ns/entailment/> "
            + "prefix dawgt: <http://www.w3.org/2001/sw/DataAccess/tests/test-dawg#> "
            + "select  * where {"
            + "?x mf:action ?a "
            + "optional {?x mf:name ?nameTest} "
            + "optional {?x rdfs:comment ?commentTest} "
            + "minus {?x dawgt:approval dawgt:Withdrawn}"
            + "optional {?a ut:request ?q} "
            + "optional {?a ut:data ?d}"
            + "optional {?a ut:graphData [?p ?g; rdfs:label ?name] "
            + "filter(?p = ut:data || ?p = ut:graph) } "
            + "optional {?x sd:entailmentRegime ?ent}"
            + "optional {?x mf:result ?ee filter(! exists {?ee ?qq ?yy}) }"
            + "optional {?x mf:result [ut:data ?r] }"
            + "optional {?x mf:result [ut:graphData [rdfs:label ?nres; ?pp ?gr ]] "
            + "filter(?pp = ut:data || ?pp = ut:graph) }"
            + "optional {?x rdf:type ?t}"
            + "{?man rdf:first ?x} "
            + "} "
            + "group by ?x order by ?q ";

    public static Stream<TestDescription>  build(String path) throws IOException {
        DraftIterator dit = new DraftIterator();
        Stream<TestDescription> fullNamesDir = dit.listEntries(path)
                .filter(
                        s -> Files.isDirectory(Paths.get(s)) &&
                                Files.exists(Paths.get(s, "manifest.ttl"))
                )
                .flatMap(
                        (String manifestDir) -> {
                            Path manifestFilename = Paths.get(manifestDir, "manifest.ttl");
                            Mappings maps = dit.getTests(manifestFilename, false);
                            ArrayList<TestDescription> tests = new ArrayList<>();
                            for (Mapping map : maps) {
                                tests.add(new TestDescription(map));
                            }
                            ;
                            return tests.stream();
                        });
        return fullNamesDir;
    }

    private static String ensureEndsWithSeparator(String path) {
        if (path.endsWith(String.valueOf(File.separatorChar))) {
            return path;
        } else {
            return path + File.separatorChar;
        }
    }

    /**
     * @return A list of all entries found in path (files, directories, links, etc.)
     */
    private Stream<String> listEntries(String path) throws IOException {
        path = ensureEndsWithSeparator(path);
        File jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        ArrayList<String> results = new ArrayList<String>();

        if (jarFile.isFile()) {  // Run with JAR file
            final JarFile jar = new JarFile(jarFile);
            final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
            while (entries.hasMoreElements()) {
                final String name = entries.nextElement().getName();
                if (name.startsWith(path + "/")) { //filter according to the path
                    results.add(name);
                }
            }
            jar.close();
        } else { // Run with IDE
            ClassLoader classLoader = getClass().getClassLoader();
            final URL url = classLoader.getResource(path);
            if (url != null) {
                try {
                    final File apps = new File(url.toURI());
                    for (File app : apps.listFiles()) {
                        results.add(app.getPath());
                    }
                } catch (URISyntaxException ex) {
                    // never happens
                }
            }
        }
        return results.stream();
    }

    private Mappings getTests(Path manifestFilename, boolean update) {
        try {
            Graph g = Graph.create();
            Load load = Load.create(g);
            load.loadWE(manifestFilename.toString());
            QueryProcess exec = QueryProcess.create(g);
            exec.setListGroup(true);

            String qman = (update) ? READ_TEST_WITH_UPDATE : READ_TEST_WITHOUT_UPDATE;

            Mappings res = exec.query(qman);
            System.out.println("** NB test: " + res.size());

            return res;
        } catch (EngineException | LoadException e) {
            System.out.println(manifestFilename);
            e.printStackTrace();
            throw new RuntimeException("Impossible to read the tests", e);
        }
    }

}
