package fr.inria.corese.w3c_tests;

import fr.inria.corese.core.Graph;
import fr.inria.corese.core.api.Loader;
import fr.inria.corese.core.load.Load;
import fr.inria.corese.core.load.LoadException;
import fr.inria.corese.core.query.ProducerImpl;
import fr.inria.corese.core.query.QueryProcess;
import fr.inria.corese.kgram.api.query.Producer;
import fr.inria.corese.kgram.core.Mappings;
import fr.inria.corese.sparql.exceptions.EngineException;
import fr.inria.corese.w3c.validator.W3CMappingsValidator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.stream.Stream;

public class Runner {
    public static String SPARQL11_DIR = "rdf-tests/sparql11/data-sparql11";

    public static boolean runTest(TestDescription test) {
        try {
            Graph graph = Graph.create();
            // manque gestion des entailments
            Load load = Load.create(graph);
            load.reset();
            QueryProcess exec = QueryProcess.create(graph, true);
            // for update:
            exec.setLoader(load);

            boolean result;
            if (test.getQuery().isPresent() && test.getResults().isPresent() && test.getInputStream().isPresent()) {
                load.load(test.getInputStream().get(), Loader.TURTLE_FORMAT);
                Mappings resultQuery = exec.query(test.getQuery().get());

                Producer p = ProducerImpl.create(Graph.create());
                Mappings resultExpected = null;
                resultExpected = fr.inria.corese.compiler.result.XMLResult.create(p).parse(test.getResults().get());

                W3CMappingsValidator validator = new W3CMappingsValidator();
                result = validator.validate(resultQuery, resultExpected);
            } else {
                if (!test.getQuery().isPresent()) {
                    System.out.println("query not present");
                }
                if (!test.getResults().isPresent())  {
                    System.out.println("results not present");
                }
                if (!test.getInputStream().isPresent()) {
                    System.out.println("input stream not present");
                }
                result = false;
                System.out.println("Not possible to run this test");
            }

            System.out.println("Result of the test: " + test.getName());
            System.out.println("Comment of the test: " + test.getComment());
            System.out.println("result = " + result);
            return result;
        } catch (LoadException | EngineException | URISyntaxException | ParserConfigurationException | SAXException | IOException e) {
            System.out.println("problem when reading the results of the test " + test.getName() + e);
            return false;
        }
    }

    public static void main(String... args) throws IOException, SAXException, ParserConfigurationException, EngineException {
        System.out.println("start");
        Stream<TestDescription> fullNamesDir = DraftIterator.build(SPARQL11_DIR);
        fullNamesDir.forEach(Runner::runTest);
    }
}
