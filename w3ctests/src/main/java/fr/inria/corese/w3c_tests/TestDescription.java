package fr.inria.corese.w3c_tests;

import fr.inria.corese.kgram.api.core.Node;
import fr.inria.corese.kgram.core.Mapping;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class TestDescription {
    private String testName;
    private String testComment;
    private String approval;
    private String approvedBy;

    public enum TYPE {
        POSITIVE_SYNTAX_TYPE("mf:PositiveSyntaxTest"),
        POSITIVE_SYNTAX_TYPE_11("mf:PositiveSyntaxTest11");
        private String value;

        TYPE(String _value) {
            this.value = _value;
        }

        public String getValue() {
            return value;
        }

        public static TYPE getType(String s) {
            for (TYPE t : TYPE.values()) {
                if (t.value.equals(s)) {
                    return t;
                }
            }
            return null;
        }
    }

    private TYPE type;

    public enum Entries {QUERY_URL, RESULT_URL}

    private HashMap<Entries, String> values = new HashMap<>();
    private String inputURL;
    private String resultURI;

    public TestDescription(Mapping rawData) {
//            Dataset input = new Dataset().init(rawData.getMappings(), "?name", "?g");
//            Dataset output = new Dataset().init(rawData.getMappings(), "?nres", "?gr");
        inputURL = getValue(rawData, "?d");

        testName = getValue(rawData, "?nameTest");
        testComment = getValue(rawData, "?commentTest");
        String typeString = getValue(rawData, "?t");
        type = TYPE.getType(typeString);
        String test = getValue(rawData, "?x");
        String queryURL = getValue(rawData, "?q");
        resultURI = getValue(rawData, "?r");
        String ent = getValue(rawData, "?ent");
        String fq = getValue(rawData, "?fq");


        String[] ep = getValues(rawData, "?endpoint");
        String[] ed = getValues(rawData, "?endpointdata");

        List<Node> rdt = rawData.getNodes("?rdt", true);
        List<Node> udt = rawData.getNodes("?udt", true);


        boolean isEmpty = getValue(rawData, "?ee") != null;
        putIfNotNull(values, Entries.QUERY_URL, queryURL);
        putIfNotNull(values, Entries.RESULT_URL, resultURI);
    }

    public String getName() {
        return testName;
    }

    public Optional<String> getComment() {
        return Optional.ofNullable(testComment);
    }

    public TYPE getType() {
        return type;
    }

    public Optional<InputStream> getResults() {
        if (resultURI == null) {
            return Optional.empty();
        } else {
            try {
                return Optional.of(readURI(resultURI));
            } catch (RuntimeException | URISyntaxException e) {
                return Optional.empty();
            }
        }
    }

    public Optional<String> getQuery() throws URISyntaxException {
        if (getQueryURL().isPresent()) {
            return Optional.of(readContent(readURI(getQueryURL().get())));
        } else {
            return Optional.empty();
        }
    }

    private void putIfNotNull(HashMap<Entries, String> values, Entries key, String value) {
        if (value != null) {
            values.put(key, value);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name = ").append(testName).append('\n');
        sb.append("type = ").append(type).append('\n');
        sb.append("queryURL=").append(values.get(Entries.QUERY_URL)).append('\n');
        sb.append("resultURL=").append(values.get(Entries.RESULT_URL)).append('\n');
        sb.append("data inputs = ").append((inputURL == null) ? "pas de données" : inputURL).append("\n");
        if (getResults().isPresent()) {
            sb.append("content of the result:");
            BufferedReader is = new BufferedReader(new InputStreamReader(getResults().get()));
            is.lines().forEach(s -> sb.append(s).append('\n'));
        } else {
            sb.append("no result");
        }

        return sb.toString();
    }

    private String getValue(Mapping map, String var) {
        if (map.getNode(var) != null) {
            return map.getNode(var).getLabel();
        }
        return null;
    }

    private String[] getValues(Mapping map, String var) {
        List<Node> list = map.getNodes(var, true);
        String[] fnamed = new String[list.size()];
        int j = 0;
        for (Node n : list) {
            fnamed[j++] = n.getLabel();
        }
        return fnamed;
    }

    private ArrayList<String> getValueList(Mapping map, String var) {
        List<Node> list = map.getNodes(var, true);
        ArrayList<String> fnamed = new ArrayList<>();
        for (Node n : list) {
            fnamed.add(n.getLabel());
        }
        if (list.size() > 1 ) {
            System.err.println("CAS AVEC PLUS DE UNE VALEUR");
            System.err.println(var);
            System.err.println(testName);
        }
        return fnamed;
    }

    public String getRequestURL() {
        return values.get(Entries.QUERY_URL);
    }

    public Optional<String> getQueryURL() {
        return Optional.ofNullable(values.get(Entries.QUERY_URL));
    }

    public Optional<String> getInputURL() {
        return Optional.ofNullable(inputURL);
    }
    public Optional<InputStream> getInputStream() throws URISyntaxException {
        if (getInputURL().isPresent()) {
            return Optional.of(readURI(getInputURL().get()));
        } else {
            return Optional.empty();
        }
    }

    // returns an In
    private InputStream readURI(String path) throws URISyntaxException {
        URI uri = URI.create(path);
        String root = Thread.currentThread().getContextClassLoader().getResource("").toURI().getPath();
        String subPath = uri.getPath().replace(root, "");
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(subPath);
    }

    private String readContent(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        reader.lines().forEach(sb::append);
        return sb.toString();
    }

}
