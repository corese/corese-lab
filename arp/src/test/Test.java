package test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.xml.sax.SAXException;

import fr.com.hp.hpl.jena.rdf.arp.ALiteral;
import fr.com.hp.hpl.jena.rdf.arp.ARP;
import fr.com.hp.hpl.jena.rdf.arp.AResource;
import fr.com.hp.hpl.jena.rdf.arp.StatementHandler;

public class Test implements StatementHandler {
	static String root = "/home/corby/workspace/kgengine/src/test/resources/data/";

	public static void main(String args[]){
		//ld.loadWE(root + "test/iso.rdf");
		new Test().process();
		
	}
	
	
	void process(){
		
		ARP arp;
		FileReader r;
		arp = new ARP();
		arp.setStatementHandler(this);
		
		try {
			
			r = new FileReader(root + "test/iso.rdf");
			arp.load(r);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("** Test error");
			e.printStackTrace();
		}
					
		arp = new ARP();
		arp.setStatementHandler(this);
		try {
			r = new FileReader(root + "test/utf.rdf");
			arp.load(r);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	@Override
	public void statement(AResource subj, AResource pred, AResource obj) {
		System.out.println(subj + " " + pred + " " + obj);
		
	}


	@Override
	public void statement(AResource subj, AResource pred, ALiteral lit) {
		System.out.println(subj + " " + pred + " " + lit);
		
	}

}
