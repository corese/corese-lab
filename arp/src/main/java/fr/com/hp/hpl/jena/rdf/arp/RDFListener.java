package fr.com.hp.hpl.jena.rdf.arp;

public interface RDFListener {

	public void setSource(String source);	
	public String getSource();
	
}
