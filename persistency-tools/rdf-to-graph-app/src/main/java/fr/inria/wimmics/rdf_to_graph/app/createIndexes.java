/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.inria.wimmics.rdf_to_graph.app;

import fr.inria.corese.rdftograph.RdfToGraph;

/**
 *
 * @author edemairy
 */
public class createIndexes {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage: createIndexes db_dir backend");
			System.err.print("known backend");
			for (RdfToGraph.DbDriver driver : RdfToGraph.DbDriver.values()) {
				System.err.print(driver + " ");
			}
			System.exit(1);
		}
		RdfToGraph.DbDriver driver = RdfToGraph.DbDriver.NEO4J;
		try {
			driver = RdfToGraph.DbDriver.valueOf(args[1].toUpperCase());
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}
		String dbPath = args[0];
		RdfToGraph.build().setDriver(driver).createIndex(dbPath);

	}
}