/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.inria.wimmics.rdf.to.graph.cypher;

import fr.inria.corese.core.edge.EdgeQuad;
import fr.inria.corese.kgram.api.core.Node;
import fr.inria.corese.rdftograph.RdfToGraph;
import fr.inria.corese.rdftograph.driver.GdbDriver;
import fr.inria.corese.rdftograph.driver.Neo4jDriver;
import static fr.inria.wimmics.rdf_to_bd_map.RdfToBdMap.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;
import org.apache.tinkerpop.gremlin.neo4j.structure.Neo4jVertex;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Element;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.openrdf.model.Literal;
import org.openrdf.model.Value;

/**
 *
 * @author edemairy
 */
public class CypherDriver extends GdbDriver {

	private final Neo4jDriver neo4jDriver;

	public CypherDriver() throws IOException {
		neo4jDriver = new Neo4jDriver();
	}

	@Override
	public Graph openDatabase(String dbPath) {
		return neo4jDriver.openDatabase(dbPath);
	}

	@Override
	public void closeDatabase() throws Exception {
		neo4jDriver.closeDatabase();
	}

	@Override
	public void commit() {
		neo4jDriver.commit();
	}

	@Override
	public Object createRelationship(Value sourceId, Value objectId, String predicate, Map<String, Object> properties) {
		return neo4jDriver.createRelationship(sourceId, objectId, predicate, properties);
	}

	/**
	 * Build the properties map for cypher from a RDF node.
	 *
	 * @param v
	 * @return Map to use in cypher when creating the node.
	 */
	public Map<String, Object> buildVertexProperties(Value v) {
		Map<String, Object> properties = new HashMap<>();
		switch (RdfToGraph.getKind(v)) {
			case IRI:
			case BNODE: {
				properties.put(VERTEX_VALUE, v.stringValue());
				properties.put(KIND, RdfToGraph.getKind(v));
				break;
			}
			case LITERAL: {
				Literal l = (Literal) v;
				properties.put(VERTEX_VALUE, l.getLabel());
				properties.put(TYPE, l.getDatatype().toString());
				properties.put(KIND, RdfToGraph.getKind(v));
				if (l.getLanguage().isPresent()) {
					properties.put(LANG, l.getLanguage().get());
				}
				break;
			}
			case LARGE_LITERAL: {
				Literal l = (Literal) v;
				properties.put(VERTEX_VALUE, Integer.toString(l.getLabel().hashCode()));
				properties.put(VERTEX_LARGE_VALUE, l.getLabel());
				properties.put(TYPE, l.getDatatype().toString());
				properties.put(KIND, RdfToGraph.getKind(v));
				if (l.getLanguage().isPresent()) {
					properties.put(LANG, l.getLanguage().get());
				}
				break;
			}
		}
		return properties;
	}

	@Override
	public Function<GraphTraversalSource, Iterator<? extends Element>> getFilter(String key, String s, String p, String o, String g) {
		Function<GraphTraversalSource, Iterator<? extends Element>> filter;
		ArrayList<String> parametersList = new ArrayList<>();

		if (key.contains("S")) {
			parametersList.add(String.format("%s: \"%s\"", EDGE_S, s));
		}
		if (key.contains("P")) {
			parametersList.add(String.format("%s: \"%s\"", EDGE_P, p));
		}
		if (key.contains("O")) {
			parametersList.add(String.format("%s: \"%s\"", EDGE_O, o));
		}
		if (key.contains("G")) {
			parametersList.add(String.format("%s: \"%s\"", EDGE_G, g));
		}
		String parameters = "";
		if (!parametersList.isEmpty()) {
			StringJoiner sj = new StringJoiner(", ", "{", "}");
			parametersList.stream().forEach(sj::add);
			parameters = sj.toString();
		}
		final String finalParameters = parameters;
		filter = (GraphTraversalSource t) -> {
			GraphTraversal<Object, Neo4jVertex> result = neo4jDriver.getNeo4jGraph().cypher("MATCH (edge: rdf_edge" + finalParameters + ") return edge").select("edge");
			return result;
		};
		return filter;
	}

	@Override

	public EdgeQuad buildEdge(Element e) {
		return neo4jDriver.buildEdge(e);
	}

	@Override
	public Node buildNode(Element e) {
		return neo4jDriver.buildNode(e);
	}

	@Override
	public boolean isGraphNode(String label) {
		return neo4jDriver.isGraphNode(label);
	}

}
