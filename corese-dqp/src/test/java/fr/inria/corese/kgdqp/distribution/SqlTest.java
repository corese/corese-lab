/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.inria.corese.kgdqp.distribution;

import fr.inria.corese.sparql.exceptions.EngineException;
import fr.inria.corese.kgram.core.Mappings;
import fr.inria.corese.core.Graph;
import fr.inria.corese.core.query.QueryProcess;
import fr.inria.corese.core.load.LoadException;
import fr.inria.corese.core.print.RDFFormat;
import fr.inria.corese.core.print.XMLFormat;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.HashMap;
import org.apache.commons.lang.time.StopWatch;
import org.junit.*;

/**
 *
 * @author gaignard
 */
public class SqlTest {

    public SqlTest() throws MalformedURLException {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws EngineException, MalformedURLException, IOException {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    @Test
    @Ignore
    public void testSimpleSql() throws EngineException, LoadException {


        String sparqlSqlRemote = "PREFIX db:<jdbc:mysql://neurolog.unice.fr:3306/>"
                + "PREFIX dr:<com.mysql.jdbc.>"
                //                + "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"
                + "construct {?s <http://xmlns.com/foaf/0.1/name> ?z}"
                + "where {"
                + "{ select(sql(db:NeuroLOG_Metadata_I3S_v21, dr:Driver, 'nlogI3SDEV', 'nlogserv', 'SELECT Dataset.dataset_id, Dataset.Study_study_id, Dataset.dataset_creation_date_time FROM Dataset') as (?x, ?y, ?z)) where {} } ."
                + "{ select(uri(?x) as ?s) where {}}"
                + "}";

        String sparqlSqlLocal = "PREFIX db:<jdbc:mysql://localhost:8889/>"
                + "PREFIX dr:<com.mysql.jdbc.>"
                //                + "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"
                + "construct {?s <http://xmlns.com/foaf/0.1/name> ?z}"
                + "where {"
                + "{ select(sql(db:NeuroLOG_Metadata_I3S, dr:Driver, 'root', 'root', 'SELECT Dataset.dataset_id, Dataset.Study_study_id, Dataset.dataset_creation_date_time FROM Dataset') as (?x, ?y, ?z)) where {} } ."
                + "{ select(uri(?x) as ?s) where {}}"
                + "}";

        String testSparql = "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
                + "PREFIX dr:<com.mysql.jdbc.>"
                + "PREFIX db:<jdbc:mysql://neurolog.unice.fr:3306/>"
                + "construct  { ?dataset <http://www.irisa.fr/visages/team/farooq/ontologies/linguistic-expression-owl-lite.owl#has-for-name> ?dsName } "
                + "where { "
                //                + " { select(sql(<jdbc:mysql://neurolog.unice.fr:3306/NeuroLOG_Metadata_I3S_v21>, <com.mysql.jdbc.Driver>, 'nlogI3SDEV', 'nlogserv', 'SELECT Dataset.dataset_id, Dataset.Study_study_id FROM Dataset') as (?x, ?dsName)) where {} } ."
                + " { select(sql(db:NeuroLOG_Metadata_I3S_v21, dr:Driver, 'nlogI3SDEV', 'nlogserv', 'SELECT Dataset.dataset_id, Dataset.Study_study_id FROM Dataset') as (?x, ?dsName)) where {} } ."
                + "{ select(uri(?x) as ?dataset) where {}} }";

        Graph graph = Graph.create();
        QueryProcess exec = QueryProcess.create(graph);

//        Mappings map = exec.query(sparqlSqlRemote);
        Mappings map = exec.query(sparqlSqlRemote);

        XMLFormat xmlF = XMLFormat.create(map);
        System.out.println(xmlF);
        System.out.println("");

        RDFFormat rdfF = RDFFormat.create(map);
        System.out.println(rdfF);

    }

    @Test
    @Ignore
    public void testExtractDSid() {
        String url = "<http://neurolog.techlog.anr.fr/data.rdf#dataset-GIN-SS-122>";
        url = url.substring(url.lastIndexOf("#dataset-")+9);
        url = url.substring(0,url.lastIndexOf(">"));
        System.out.println(url);
    }
}
