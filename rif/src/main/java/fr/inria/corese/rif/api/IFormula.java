package fr.inria.corese.rif.api;

/** A formula refers to a rule premise, the right part of an implication. */
public interface IFormula extends IConnectible {
	
}
